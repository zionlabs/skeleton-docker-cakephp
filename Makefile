# fichier /Makefile
include .env.local

## build			construit tous les conteneurs
build:
	 docker-compose --env-file .env.local build

## build-nocache		construit tous les conteneurs (suppression du cache)
build-nocache:
	 docker-compose --env-file .env.local build --no-cache

## clean			arrête et supprime les conteneurs
clean:
	docker-compose --env-file .env.local kill
	docker-compose --env-file .env.local down --volumes --remove-orphans


## help			extrait les commentaires qui commencent par "##" (crédit Software Carpentry)
help : Makefile
	@sed -n 's/^##//p' $<

## reset			arrête, supprime, reconstruit et relance les conteneurs
reset: stop clean build start

## reset-nocache		arrête, supprime, reconstruit et relance les conteneurs (suppression du cache)
reset-nocache: stop clean build-nocache start

## restart		redémarre tout
restart: stop start

## start			démarre tous les conteneurs (et les construit si besoin)
start:
	docker-compose -p ${APP_NAME} --env-file .env.local up -d
	@echo Projet ${APP_NAME} disponible sur http://localhost

## stop			arrête tous les conteneurs
stop:
	docker-compose --env-file .env.local stop