<!-- fichier /src/index.php -->

<h1>Test de connexion à la base de données</h1>
<hr style="margin:1em 0 2em">
<?php
$nomServeur     = getenv('APP_NAME').'-db';
$utilisateurBdd = getenv('DB_USER');
$motDePasseBdd  = getenv('DB_PASSWORD');


// création de la connexion
$connexion = new mysqli($nomServeur, $utilisateurBdd, $motDePasseBdd);

// Vérification de la connexion
if ($connexion->connect_error) {
    die("Échec de la connexion: " . $connexion->connect_error);
}
echo "Connexion réussie";