# Skeleton Docker PHP


## 🚀 Démarrage rapide TL;DR
```
git clone https://gitlab.com/zionlabs/skeleton-docker-cakephp.git votrePropreDossier
cd votrePropreDossier
cp .env.exemple .env.local
make start
```

## Stack
Environnement Docker Compose avec conteneurs 
- Apache PHP
- Mysql


## Usage
Vous pouvez arrêter avec 
```
make stop
```
supprimer les conteneurs avec
```
make clean
```

tout réinitialiser avec 
```
make reset
```

et voir toutes les commandes avec 
```
make help
```
